package br.com.dbccompany.hackathon.Reader;

import br.com.dbccompany.hackathon.Entity.Dados;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class Reader{

    @Bean
    public MultiResourceItemReader<Dados> multiResourceItemReader(Resource[] inputResources) {
        MultiResourceItemReader<Dados> resourceItemReader = new MultiResourceItemReader<Dados>();
        resourceItemReader.setResources(inputResources);
        resourceItemReader.setDelegate(readerDaClasse());
        return resourceItemReader;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Bean
    public FlatFileItemReader<Dados> readerDaClasse() {
        FlatFileItemReader<Dados> reader = new FlatFileItemReader<Dados>();

        reader.setLineMapper(new DefaultLineMapper() {
            {
                setLineTokenizer(new DelimitedLineTokenizer() {
                    {
                        setDelimiter("ç");
                        setNames("id", "primeiraInfo", "segundaInfo", "terceiraInfo");
                    }
                });
                setFieldSetMapper(new BeanWrapperFieldSetMapper<Dados>() {
                    {
                        setTargetType(Dados.class);
                    }
                });
            }
        });
        return reader;
    }

}
