package br.com.dbccompany.hackathon.BatchConfig;

import br.com.dbccompany.hackathon.Entity.Dados;
import br.com.dbccompany.hackathon.Entity.DadosProcessados;
import br.com.dbccompany.hackathon.Entity.DadosWriter;
import br.com.dbccompany.hackathon.Reader.Reader;
import br.com.dbccompany.hackathon.Writer.Writer;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    @Qualifier(value = "processador")
    private ItemProcessor<Dados, DadosWriter> processador;

    @Autowired
    private Reader multiResourceItemReader;

    @Autowired
    private Writer writer;

    @Value("data/*.dat")
    private Resource[] inputResources;

    @Bean
    public Job job() {
        return jobBuilderFactory
                .get("job")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1").<Dados, DadosWriter>chunk(1)
                .reader(multiResourceItemReader.multiResourceItemReader(inputResources))
                .processor(processador)
                .writer(writer.writerDaClasse())
                .build();
    }

}