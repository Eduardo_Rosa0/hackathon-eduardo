package br.com.dbccompany.hackathon.Writer;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ProcessorTest {

    /*@Test
    void processRetornaNumeroDeVendedores() throws Exception {
        Processor processor = new Processor();
        DadosProcessados dadosProcessados;
        dadosProcessados = processor.process(new Employee( "001", "1234567981324", "Pedro", "5000" ));
        dadosProcessados = processor.process(new Employee( "001", "3245678865434", "Paulo", "4000.99" ));
        dadosProcessados.equals(new DadosProcessados(0, 2, null, null));
    }

    @Test
    void processRetornaNumeroDeClientes() throws Exception {
        Processor processor = new Processor();
        DadosProcessados dadosProcessados;
        dadosProcessados = processor.process(new Employee("002", "2345675434544345", "JoseDaSilva", "Rural"));
        dadosProcessados = processor.process(new Employee("002", "2345675433444345", "EduardoPereira", "Rural"));
        dadosProcessados = processor.process(new Employee("002", "2345675433444545", "JosePereira", "Rural"));
        dadosProcessados.equals(new DadosProcessados(3,0,null,null));
    }

    @Test
    void processRetornaIdMaiorVenda () throws Exception {
        Processor processor = new Processor();
        DadosProcessados dadosProcessados;
        dadosProcessados = processor.process(new Employee("003", "10", "[1-10-100,2-30-2.50,3-40-3.10]", "Pedro"));
        dadosProcessados = processor.process(new Employee("003", "08", "[1-34-10,2-33-1.50,3-40-0.10]", "Paulo"));
        dadosProcessados.equals(new DadosProcessados(0, 0, "10", null));
    }

    @Test
    void processRetornaPiorVendedor () throws Exception {
        Processor processor = new Processor();
        DadosProcessados dadosProcessados;
        dadosProcessados = processor.process(new Employee( "001", "1234567981324", "Pedro", "5000" ));
        dadosProcessados = processor.process(new Employee( "001", "3245678865434", "Paulo", "4000.99" ));
        dadosProcessados = processor.process(new Employee("003", "10", "[1-10-100,2-30-2.50,3-40-3.10]", "Pedro"));
        dadosProcessados = processor.process(new Employee("003", "08", "[1-34-10,2-33-1.50,3-40-0.10]", "Paulo"));
        dadosProcessados.equals(new DadosProcessados(0, 2, "10", "Paulo"));
    }

    @Test
    void processRetornaTodasInformacoesCorretasDoInputDeDadosDeReferencia () throws Exception {
        Processor processor = new Processor();
        DadosProcessados dadosProcessados;
        dadosProcessados = processor.process(new Employee( "001", "1234567981324", "Pedro", "5000" ));
        dadosProcessados = processor.process(new Employee( "001", "3245678865434", "Paulo", "4000.99" ));
        dadosProcessados = processor.process(new Employee("002", "2345675434544345", "JoseDaSilva", "Rural"));
        dadosProcessados = processor.process(new Employee("002", "2345675433444345", "EduardoPereira", "Rural"));
        dadosProcessados = processor.process(new Employee("003", "10", "[1-10-100,2-30-2.50,3-40-3.10]", "Pedro"));
        dadosProcessados = processor.process(new Employee("003", "08", "[1-34-10,2-33-1.50,3-40-0.10]", "Paulo"));
        dadosProcessados.equals(new DadosProcessados(2, 2, "10", "Paulo"));
    }*/
}
