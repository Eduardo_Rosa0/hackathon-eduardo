# Readme

## Trata-se de uma aplicação de processamento de arquivos **.dat**. Ela irá ler dados de Vendedores, suas vendas e os clientes que compraram.
## Após o processamento dessas informações irá disponibilizar um relatório no formato **.dat** com as seguintes informações:
  - ### Quantidade de clientes no arquivo de entrada
  - ### Quantidade de vendedor no arquivo de entrada
  - ### ID da venda mais cara
  - ### O pior vendedor 


---

    Para o bom funcionamento da aplicação, favor seguir as instruçoes.

## Importante 
- ### O arquivo deve seguir uma 'layout' especifico para cada tipo de informação.
- ### Todo arquivo deve ter uma linha para cada tipo de 'layout'.
  
### São eles:
  1. Dados do vendedor
      - "001`ç`CPF`ç`Nome`ç`Salario"
   1. Dados do cliente
      - "002`ç`CNPJ`ç`Nome`ç`Área de negócio"
   2. Dados de vendas
      - "003`ç`Vendedor ID`ç`['Item ID'-'Item Quantidade'-'Valor do Item']`ç`Salesman Nome"
  
  **Observe que a linha `Dados de venda` pode ter mais de um `Item` dentro do conchete `"[]"`, neste caso os Itens devem ser separados por uma vírgula `","` .** 
  
  
### Onde `ç` é o delimitador de informação. Conforme o exemplo: 

    001ç1234567891234çPedroç50000
    001ç3245678865434çPauloç40000.99
    002ç2345675434544345çJose da SilvaçRural
    002ç2345675433444345çEduardo PereiraçRural
    003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çPedro
    003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çPaulo

---

> ## Intruções:

- ## Criando a pasta onde deixaremos os arquivos `dat` para serem devidamente processados e obtermos o relatório final.
  1. Abra seu terminal ( `Ctrl+t` )
  2. Navegue até sua pasta pessoal ( ` cd ~` )
  3. Crie uma pasta chama `data` ( `mkdir data` )
  4. Acesse a pasta `data` criada no passo anterior ( `cd data` )
  5. Crie uma pasta chamada `in` e outra chamada `out` ( `mkdir in; mkdir out` )

---

  > ## Configuração Docker
  > ### \* Necessário ter o Docker intalado em sua máquina, [help](https://docs.docker.com/get-started/overview/).

  - ## Inicializando a aplicação em sua maquina.
    1. Abra seu terminal ( `Ctrl+t` )
    2. Navegue até sua pasta pessoal ( `cd ~` )
    3. Crie uma pasta chamada `app` ( `mkdir app` )
    4. Acesse a pasta ( `cd app` )
    5. Clone o [repositório oficial do projeto](https://bitbucket.org/BryanLopes77/hackathon/src/master/). ( `git clone [URL]` ) 
    6. Execute o comando para criar um ambiente docker para a aplicação funcionar. ( `TO-DO` )
    7. Verifique se a aplicação esta executando corretamente dentro do container docker. ( `TO-DO` )



---
> ## Desenvolvedores:
  - [Eduardo Rosa (paoconha)](www.linkedin.com/in/eduardo-rosa-dos-santos).
  - [Bryan Lopes (é o braia)](https://www.linkedin.com/in/bryan-lopes-b8a87618b?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3B9lisMhHlQFukgVGdPK3cGA%3D%3D).
  - [Kevin Brandolff (não quer vir não!)](https://www.linkedin.com/in/kevinbrandolff?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3B3CpewTGSRt%2B7gmY8zCjSgg%3D%3D).
  - [Rafael Bromberg (o outro)](https://www.linkedin.com/in/rafael-bromberg-antunes-030a501ab?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BKDwoXaSSQqqw%2FlUyfT7oyg%3D%3D).
---